Vue.component('cm-liquidacion', {
    data() {
        return { 
			cuLiquidacionStore: cuLiquidacionStore.data,
			expediente_show: false,
			expSearch: '',
            informe_url: host+'/liquidacion/emitir?id_expediente='
        }
    },
    methods: {
        searchExpedientes() { 
        	$('.acotarLoad').show();
        	cuLiquidacionStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        selectExpediente(id_expediente, ano_expediente) {
        	$('.acotarLoad').show();
        	cuLiquidacionStore.methods.seleccionarExpediente(id_expediente, ano_expediente);
        	this.expediente_show = true;
        },
        return_all() {
        	this.expediente_show = false;	
        }
    },
    created() {
        //rolStore.methods.created();
        cuLiquidacionStore.methods.loadExpedientes();
    }
});