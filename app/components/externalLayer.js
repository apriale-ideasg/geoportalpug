Vue.component('cm-externallayer', {
    data() {
        return {
            controlCapaStore: controlCapaStore.data
        }
    },
    methods: {
        changeLayer(nomb_capa){
            this.controlCapaStore.capaMap.forEach(element => {
                if (element.nombre == nomb_capa) {
                    element.capa.setVisible(!element.capa.getVisible());
                    controlCapaStore.methods.legendShow(nomb_capa, element.capa.getVisible());
                }
            });
        }
    },
    created() {
        $('.externalLayerLoad').show();
        if (loginStore.data.userLogged == true)
            controlCapaStore.methods.showList(loginStore.data.user.tipo_usuario);
    }
});