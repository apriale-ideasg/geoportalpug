Vue.component('cm-acotar', {
    data() {
        return { 
			cuAcotarStore: cuAcotarStore.data,
			expediente_show: false,
			expSearch: ''
        }
    },
    methods: {
        searchExpedientes() { 
        	$('.acotarLoad').show();
        	cuAcotarStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        selectExpediente(id_expediente) {
        	$('.acotarLoad').show();
        	cuAcotarStore.methods.seleccionarExpediente(id_expediente);
        	this.expediente_show = true;
        },
        return_all() {
        	this.expediente_show = false;	
        }
    },
    created() {
        //rolStore.methods.created();
        cuAcotarStore.methods.loadExpedientes();
    }
});