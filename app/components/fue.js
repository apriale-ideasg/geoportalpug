Vue.component('cm-fue', {
    data() {
        return {
            fueStore: fueStore.data,
            cuMaestrosStore: cuMaestrosStore.data,
            tipo_tramite: [],
            tipo_obra: [],
            modalidad_aprob: [],
            modalidad_aprob_opt: [],
            anexos: [],
            departamentos: [],
            provincias: [],
            distritos: [],
            estado_civil: [],
            tipo_via: [],
            doc_adjuntos: [],
            edificacion: [],
            te_edificacion: {
                concepto : []
            },
            solicitud : {},
            administrado : {},
            proyecto : {
                pisos : []
            },
            valor_obra : [],
            terreno: {
                departamento : "JUNIN",
                provincia : "HUANCAYO",
                distrito : "EL TAMBO"
            },
            adjuntos: {
                documento : [],
                doc_val : [],
                doc_desc: []
            },
            observaciones: '',
            proyectista: [],
            otro_proyectista: [],
            message: {
                send: false,
                state: '',
                value: '',
                title: ''
            }
        }
    },
    methods: {
        changeModalidad() {
            const modalidad = this.modalidad_aprob.find( modalidad => modalidad.id_modalidad_aprobacion == this.solicitud.id_modalidad );
            this.modalidad_aprob_opt = modalidad.hijos;
        },
        changePerEmp() {
            console.log(this.administrado.tipo_persona);
        },
        addPiso() {
            this.proyecto.pisos.push({});
        },
        addValorObra(){
            this.valor_obra.push({});
        },
        addProyectista(){
            this.otro_proyectista.push({});
        },
        continueSolicitud(){
            console.log(this.solicitud);
        },
        insertFue(){

            $('.fueLoad').show();

            let anexos_send = [];
            let concepto_edif = [];
            let documentos = [];

            this.solicitud.anexos.forEach( el => {
                anexos_send.push({"anexo": el});
            })
            this.te_edificacion.concepto.forEach( (el, index) => {
                concepto_edif.push({"concepto": index, "valor": el});
            })

            this.adjuntos.documento.forEach( (el, index) => {
                documentos.push({"doc": index, "valor": el, "cantidad": this.adjuntos.doc_val[index], "descripcion": this.adjuntos.doc_desc[index] });
            })

            let expediente = {
                "solicitud" : this.solicitud,
                "administrado" : this.administrado,
                "terreno" : this.terreno,
                "edificacion" : this.te_edificacion,
                "adjuntos" : this.adjuntos,
                "proyecto" : this.proyecto,
                "valor_obra" : this.valor_obra,
                "observaciones" : this.observaciones,
                "proyectistas" : this.proyectista.concat(this.otro_proyectista)
            };
            expediente = JSON.parse( JSON.stringify(expediente) );

            //expediente.adjuntos.documento = [];
            expediente.solicitud.anexos = anexos_send;
            expediente.edificacion.concepto = concepto_edif;
            expediente.adjuntos.documento = documentos;
            expediente.adjuntos.doc_val = undefined;
            expediente.adjuntos.doc_desc = undefined;

            expediente.proyectistas.forEach( (el, index) => {
                el.id_tipo_proyectista = 1;
                el.especificar = "";
            })

            console.log(expediente);
            //debugger;
            fueStore.methods.insertFue(expediente).then( (exito, fallo) => {
                if(exito.success == true ){
                    this.message.send = true;
                    this.message.state = 'success';
                    this.message.value = 'El formulario fue registrado exitosamente';
                    this.message.title = '!Felicidades!';
                }
                else if(exito.success == false){
                    this.message.send = true;
                    this.message.state = 'warning';
                    this.message.value = 'Se presentaron errores al registrar el formulario, intentelo nuevamente';
                    this.message.title = 'Ups!';
                }
            });
        },
        returnFue(){
          this.message.send = false;
        }
    },
    computed: {

    },
    created() {
        var getTipoTramite = fueStore.methods.getTipoTramite();
        getTipoTramite.then( () => {
            this.tipo_tramite = checkNivel(0, null, this.fueStore.tipo_tramite);
        })

        var getTipoObra = fueStore.methods.getTipoObra();
        getTipoObra.then( () => {
            this.tipo_obra = checkNivel(0, null, this.fueStore.tipo_obra);
        })

        var getModalidadAprobacion = fueStore.methods.getModalidadAprobacion();
        getModalidadAprobacion.then( () => {
            this.modalidad_aprob = checkNivel(0, null, this.fueStore.modalidad_aprobacion);
        })

        fueStore.methods.getAdjuntos().then( () => {
            this.doc_adjuntos = this.fueStore.doc_adjuntos;
        });

        fueStore.methods.getEdificacion().then( () => {
            this.edificacion = this.fueStore.edificacion;
        });

        fueStore.methods.getAnexos().then( () => {
            this.anexos = this.fueStore.anexos;
        });

        cuMaestrosStore.methods.getDepartamento().then();
        cuMaestrosStore.methods.getProvincias().then();
        cuMaestrosStore.methods.getDistritos().then();
        cuMaestrosStore.methods.getEstadoCivil().then();
        cuMaestrosStore.methods.getTipoVia();

        this.solicitud.anexos = [];
        this.administrado.persona = {};
        this.administrado.domicilio = {};
        this.administrado.conyuge = {};
        this.administrado.apoderado = {};

        this.proyecto.pisos.push({});
        this.valor_obra.push({});

        this.edificacion.concepto = [];

        this.proyectista[0] = {};
        this.proyectista[1] = {};
        this.proyectista[2] = {};
        this.proyectista[3] = {};

        this.otro_proyectista.push({});
        //$('.fueLoad').hide();

        $('.nav-tabs').scrollingTabs();

    }
});

function checkNivel(nivel, padre, array){
    var newArray = [];
    array.forEach( el => {
        if(el.nivel == nivel && el.padre == padre){
            el.hijos = [];

            var hijos_tipo_tramite = checkNivel(el.nivel + 1, el.id_tipo_tramite, array);
            var hijos_tipo = checkNivel(el.nivel + 1, el.id_tipo, array);
            var hijos_modalidad = checkNivel(el.nivel + 1, el.id_modalidad_aprobacion, array);

            if(hijos_tipo_tramite.length > 0)
                el.hijos = hijos_tipo_tramite;
            else if(hijos_tipo.length > 0)
                el.hijos = hijos_tipo;
            else if(hijos_modalidad.length > 0)
                el.hijos = hijos_modalidad;

            newArray.push(el);
        }
    })
    return newArray;
}
