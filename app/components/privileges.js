//import rolStore from "../stores/rolStore"
//import privilegioStore from "../stores/privilegioStore"
Vue.component('cm-privileges', {
    data() {
        return {
            rolStore: rolStore.data,
            privilegioStore: privilegioStore.data,
            rolSelect: '',
            disabledPriv: 'disabled'
        }
    },
    methods: {
        savePrivilege() {
            var checksTabla = [];
            $('.privilegioLoad').show();
            $('#privileges-list tbody tr').each(function () {
                var privilegesTxt = "";
                var tabla = $(this).children('.table-name')[0].innerText;
                if ($(this).find('.selectr')[0].checked || $(this).find('.updater')[0].checked || $(this).find('.deleter')[0].checked || $(this).find('.insertr')[0].checked) {
                    if ($(this).find('.selectr')[0].checked) { privilegesTxt += "SELECT,"; }
                    if ($(this).find('.insertr')[0].checked) { privilegesTxt += "INSERT,"; }
                    if ($(this).find('.updater')[0].checked) { privilegesTxt += "UPDATE,"; }
                    if ($(this).find('.deleter')[0].checked) { privilegesTxt += "DELETE,"; }
                    privilegesTxt = privilegesTxt.substring(0, privilegesTxt.length - 1);

                    checksTabla.push({ "tabla": tabla, "privilege": privilegesTxt });
                }
            })
            privilegioStore.methods.savePrivilegioRol(checksTabla, this.rolSelect);
        },
        changeRol() {
            $('.privilegioLoad').show();
            if (this.rolSelect != ""){
                this.disabledPriv = '';
                privilegioStore.methods.privilegioRol(this.rolSelect);
            }
            else{
                this.disabledPriv = 'disabled';
                privilegioStore.methods.created();
            }
        }
    },
    created() {
        if (loginStore.data.userLogged == true && loginStore.data.user.tipo_usuario == "gl_administrador")
            privilegioStore.methods.created();
    }
});