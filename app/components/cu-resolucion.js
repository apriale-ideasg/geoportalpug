var draw;

Vue.component('cm-resolucion', {
    data() {
        return { 
			cuResolucionStore: cuResolucionStore.data,
			expediente_show: false,
			expSearch: ''            
        }
    },
    methods: {
        searchExpedientes() { 
        	$('.acotarLoad').show();
        	cuResolucionStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        selectExpediente(id_expediente, ano_expediente) {
        	$('.acotarLoad').show();
        	cuResolucionStore.methods.seleccionarExpediente(id_expediente, ano_expediente);
        	this.expediente_show = true;
        },
        return_all() {
        	this.expediente_show = false;	
        }
    },
    created() {
        //rolStore.methods.created();
        cuResolucionStore.methods.loadExpedientes();
    }
});