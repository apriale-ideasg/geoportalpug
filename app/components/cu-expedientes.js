Vue.component('cm-expedientes', {
    data() {
        return { 
			cuExpedientesStore: cuExpedientesStore.data,
			expediente_state: 0,
            expediente_substate: 0,
            expediente: {},
            observaciones_list: [],
			expSearch: '',
            proyecto: {
                pisos: []
            },
            informe: {
                administrativo: [],
                pisos: []
            },
            state: {
                class: ''
            },
        }
    },
    methods: {
        searchExpedientes() { 
        	$('.tecnicoLoad').show();
        	cuExpedientesStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        selectExpediente(id_expediente, ano_expediente) {
        	$('.tecnicoLoad').show();
        	cuExpedientesStore.methods.seleccionarExpediente(id_expediente, ano_expediente);
        	this.expediente_state = 1;
        },
        return_all() {
            if(this.expediente_substate == 0)
        	   this.expediente_state = 0;
            else
                this.expediente_substate = 0;
        },
        programacion(id_expediente) {
            $('.tecnicoLoad').show();
            cuExpedientesStore.methods.seleccionarExpediente(id_expediente);
            this.expediente_state = 2;
        },
        registerProgramacion() {
            var getProgramacion = cuExpedientesStore.methods.registerProgramacion(this.expediente);

            getProgramacion.then( () => {
                //this.expediente_state = 1;
            })
        },
        observaciones(id_expediente) {
            $('.tecnicoLoad').show();
            cuExpedientesStore.methods.seleccionarExpediente(id_expediente);
            this.expediente_state = 3;
        },
        showObservacion(){
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            $('.tecnicoLoad').show();
            this.expediente_substate = 2;
            cuExpedientesStore.methods.loadObservaciones(id_expediente);
        },
        addObservacion() {
            this.cuExpedientesStore.observaciones.push({"observacion" : ''});
        },
        deleteObserv(index){
            this.cuExpedientesStore.observaciones.splice(index,1);
        },
        editObservacion(){
            this.expediente_substate = 1;
            this.state.class="";
            if(this.cuExpedientesStore.observaciones.length == 0)
                this.cuExpedientesStore.observaciones.push({"observacion" : ''});
        },
        registerObservaciones() {
            let obs = [];
            this.cuExpedientesStore.observaciones.forEach( (el) => {
                if(el.observacion != "")
                    obs.push({'observacion' : el.observacion});
            });
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            var inserttObs = cuExpedientesStore.methods.registerObservaciones(obs, id_expediente);

            inserttObs.then( (data) => {
                if(data.success)
                    this.expediente_substate = 2;
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        },
        cancelObservaciones(){
            this.expediente_substate = 2;
            this.state.class="";
            cuExpedientesStore.methods.loadObservaciones(id_expediente);
        },
        showVerificacionAd(){
            $('.tecnicoLoad').show();
            cuExpedientesStore.methods.seleccionarVerificacion(this.cuExpedientesStore.select_expediente.id_expediente);
            this.expediente_substate = 4;
            this.state.class="";
        },
        editVerificacion(){
            this.expediente_substate = 3;
            this.state.class="show";
        },
        cancelVerificacion(){
            $('.tecnicoLoad').show();
            cuExpedientesStore.methods.seleccionarVerificacion(this.cuExpedientesStore.select_expediente.id_expediente);
            this.expediente_substate = 4;
            this.state.class="";
        },
        registerVerificacion(){
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            var informeProm = cuExpedientesStore.methods.registerVerificacion(id_expediente);
            informeProm
            .then( (data) => {
                if(data.success)
                    this.expediente_substate = 4;
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        },
        informar() {
            this.expediente_state = 4;
            $('.tecnicoLoad').hide();
        },
        registerInforme() {
            let informe = {
                "verificacion" : cuExpedientesStore.data.verificacion
            }

            var informeProm = cuExpedientesStore.methods.registerInforme(informe);
        },
        stateClose(){
            this.state.class="";
        },


        showDocPresentados(){
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            $('.tecnicoLoad').show();
            this.expediente_substate = 10;
            this.state.class="";
            cuExpedientesStore.methods.loadDocPresentados(id_expediente);
        },
        addDocumentos() {
            this.cuExpedientesStore.doc_presentados.push({"descripcion" : ''});
        },
        deleteDoc(index){
            this.cuExpedientesStore.doc_presentados.splice(index,1);
        },
        editDocumentos(){
            this.expediente_substate = 9;
            this.state.class="";
            if(this.cuExpedientesStore.doc_presentados.length == 0)
                this.cuExpedientesStore.doc_presentados.push({"descripcion" : ''});
        },
        registerDocumentos() {
            let obs = [];
            this.cuExpedientesStore.doc_presentados.forEach( (el) => {
                if(el.descripcion != "")
                    obs.push({'documento' : el.descripcion});
            });
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            var insertDocs = cuExpedientesStore.methods.registerDocPresentados(obs, id_expediente);

            insertDocs.then( (data) => {
                if(data.success)
                    this.expediente_substate = 10;
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        },

        showAntecedentes(){
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            $('.tecnicoLoad').show();
            this.expediente_substate = 12;
            this.state.class="";
            cuExpedientesStore.methods.loadAntecedentes(id_expediente);
        },
        addAntecedente() {
            this.cuExpedientesStore.antecedentes.push({"descripcion" : ''});
        },
        deleteAntecedente(index){
            this.cuExpedientesStore.antecedentes.splice(index,1);
        },
        editAntecedentes(){
            this.expediente_substate = 11;
            this.state.class="";
            if(this.cuExpedientesStore.antecedentes.length == 0)
                this.cuExpedientesStore.antecedentes.push({"descripcion" : ''});
        },
        registerAntecedente() {
            let obs = [];
            this.cuExpedientesStore.antecedentes.forEach( (el) => {
                if(el.descripcion != "")
                    obs.push({'antecedente' : el.descripcion});
            });
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            var insertAnt = cuExpedientesStore.methods.registerAntecedente(obs, id_expediente);

            insertAnt.then( (data) => {
                if(data.success)
                    this.expediente_substate = 12;
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        },

        showAntecedentes(){
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            $('.tecnicoLoad').show();
            this.expediente_substate = 12;
            this.state.class="";
            cuExpedientesStore.methods.loadAntecedentes(id_expediente);
        },
        addAntecedente() {
            this.cuExpedientesStore.antecedentes.push({"descripcion" : ''});
        },
        deleteAntecedente(index){
            this.cuExpedientesStore.antecedentes.splice(index,1);
        },
        editAntecedentes(){
            this.expediente_substate = 11;
            this.state.class="";
            if(this.cuExpedientesStore.antecedentes.length == 0)
                this.cuExpedientesStore.antecedentes.push({"descripcion" : ''});
        },
        registerAntecedente() {
            let obs = [];
            this.cuExpedientesStore.antecedentes.forEach( (el) => {
                if(el.descripcion != "")
                    obs.push({'antecedente' : el.descripcion});
            });
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            var insertAnt = cuExpedientesStore.methods.registerAntecedente(obs, id_expediente);

            insertAnt.then( (data) => {
                if(data.success)
                    this.expediente_substate = 12;
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        },

        showParametrosUrb(){
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            $('.tecnicoLoad').show();
            this.expediente_substate = 6;
            this.state.class="";
            cuExpedientesStore.methods.loadParametros(id_expediente);
        },
        editParametros(){
            this.expediente_substate = 5;
        },
        cancelParametros(){
            this.expediente_substate = 6;
            this.state.class="";
        },
        registerParametros() {
            let param_proy = this.cuExpedientesStore.parametros_edif;
            let param_urb = this.cuExpedientesStore.parametros_urb;
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            var insertPar = cuExpedientesStore.methods.registerParametros(id_expediente, param_urb, param_proy);

            insertPar.then( (data) => {
                if(data.success)
                    this.expediente_substate = 6;
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        },

        showDcAreas(){
            this.expediente_substate = 7;
            this.state.class="";
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            $('.tecnicoLoad').show();
            cuExpedientesStore.methods.loadDcAreas(id_expediente);
        },
        addPisos(){
            this.cuExpedientesStore.dcAreas.push({});
        },
        editDcAreas(){
            this.expediente_substate = 8;
            this.state.class="";
            if(this.cuExpedientesStore.dcAreas.length == 0)
                this.cuExpedientesStore.dcAreas.push({
                    'piso': ""
                });
        },
        registerDcAreas() {
            let pisos = this.cuExpedientesStore.dcAreas;
            var id_expediente = this.cuExpedientesStore.select_expediente.id_expediente;
            var insertDcAreas = cuExpedientesStore.methods.registerDcAreas(id_expediente, pisos);

            insertDcAreas.then( (data) => {
                if(data.success)
                    this.expediente_substate = 7;
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        }


    },
    created() {
        //rolStore.methods.created();
        cuExpedientesStore.methods.loadExpedientes();
        //
        this.observaciones_list.push("");
    }
});