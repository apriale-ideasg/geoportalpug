Vue.component('cm-fuhu-show', {
    data() {
        return {
            fuhuStore: fuhuStore.data,
            cuMaestrosStore: cuMaestrosStore.data,
            tramite_check: 1,
            tipo_tramite: [],
            tipo_tramite_opt: [],
            tipo_obra: [],
            tipo_obra_child: [],
            tipo_obra_subchild: [],
            modalidad_aprob: [],
            modalidad_aprob_opt: [],
            anexos: [],
            estado_civil: [],
            tipo_via: [],
            doc_adjuntos: [],

            observaciones: '',
            solicitud : {},
            administrado : {},
            terreno: {
                departamento : "JUNIN",
                provincia : "HUANCAYO",
                distrito : "EL TAMBO"
            },
            proyecto: {},
            proyectistas_cap: [],
            proyectistas_cip: [],
            adjuntos: {
                documento : [],
                doc_val : [],
                doc_desc: []
            },
            message: {
                send: false,
                state: 'success',
                value: ''
            },
            xy_select: false,
            xy_selecting: false
        }
    },
    methods: {
        mostrar(){
            fuhuStore.data.show_id_expediente = '100562889';
            fuhuStore.methods.buscarExpediente();
        }
    },
    computed: {
    },
    created() {

        var getTipoTramite = fuhuStore.methods.getTipoTramite();
        getTipoTramite.then( () => {
            this.tipo_tramite = checkNivel(0, null, this.fuhuStore.tipo_tramite);
        })

        var getTipoObra = fuhuStore.methods.getTipoObra();
        getTipoObra.then( () => {
            this.tipo_obra = checkNivel(0, null, this.fuhuStore.tipo_obra);
        })

        var getModalidadAprobacion = fuhuStore.methods.getModalidadAprobacion();
        getModalidadAprobacion.then( () => {
            this.modalidad_aprob = checkNivel(0, null, this.fuhuStore.modalidad_aprobacion);
        })

        fuhuStore.methods.getAdjuntos().then( () => {
            this.doc_adjuntos = this.fuhuStore.doc_adjuntos;
        });

        fuhuStore.methods.getAnexos().then( () => {
            this.anexos = this.fuhuStore.anexos;
        });

        cuMaestrosStore.methods.getDepartamento().then();
        cuMaestrosStore.methods.getProvincias().then();
        cuMaestrosStore.methods.getDistritos().then();
        cuMaestrosStore.methods.getEstadoCivil().then();
        cuMaestrosStore.methods.getTipoVia();

        this.solicitud.anexos = [];
        this.administrado.persona = {};
        this.administrado.domicilio = {};
        this.administrado.conyuge = {};
        this.administrado.apoderado = {};

        this.proyectistas_cap = [{},{}];
        this.proyectistas_cip = [{},{}];
    }
});
