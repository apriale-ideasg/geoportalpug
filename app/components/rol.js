Vue.component('cm-rol', {
    data() {
        return {
            rolStore: rolStore.data,
            rolName: '',
            rolClave: '',
            disabledRol: true
        }
    },
    methods: {
        nuevoRol(){
            this.disabledRol = !this.disabledRol;
            this.rolName = "";
            this.rolClave = "";
        },
        saveRol() {
            $('.rolLoad').show();
            var new_rol = {"rolname": this.rolName, "rolpass": this.rolClave};
            rolStore.methods.saveRol(new_rol);
            this.nuevoRol();
        },
        deleteRol(rolname, index) {
            var result = confirm("¿Estas seguro de eliminar el registro?");
            if (!result) {
                return false;
            }else{
                $('.rolLoad').show();
                var rol = { "rolname": rolname, "index": index };
                rolStore.methods.deleteRol(rol);
            }
        }
    },
    created() {
        if (loginStore.data.userLogged == true && loginStore.data.user.tipo_usuario == "gl_administrador")
            rolStore.methods.created();
    }
});