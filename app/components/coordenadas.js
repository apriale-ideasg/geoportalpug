var draw;

Vue.component('cm-coordenadas', {
    data() {
        return {
            coorLongitud: '',
            coorLatitud: '',
            coorZoom: ''
        }
    },
    methods: {
        showCoordenada() {
            var view = map.getView();
 
            proj4.defs('EPSG:4326');
            proj4.defs('EPSG:3857');

            var p900913 = ol.proj.get("EPSG:3857");
            var p4326 = ol.proj.get("EPSG:4326");
            
            var coord = [parseFloat(this.coorLongitud), parseFloat(this.coorLatitud)]; //[-75, -12]; //
            var LonLat = ol.proj.transform(coord, p4326, p900913)

            view.animate({
                zoom: this.coorZoom,
                center: LonLat,
                duration: 500
            });
            //view.getProjection
        }
    }
});