Vue.component('cm-asesoria-legal', {
    data() {
        return { 
			cuAsesoriaLegalStore: cuAsesoriaLegalStore.data,
			expediente_show: false,
            expediente : {},
			expSearch: '',
            state: {
                class: ''
            }
        }
    },
    methods: {
        stateClose(){
            this.state.class="";
        },
        searchExpedientes() { 
        	$('.asesoriaLoad').show();
        	cuAsesoriaLegalStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        selectExpediente(id_expediente, propiedad_verificado) {
        	$('.asesoriaLoad').show();
        	cuAsesoriaLegalStore.methods.seleccionarExpediente(id_expediente);
            if(propiedad_verificado == 0){
                this.expediente.propiedad = false;
            } else this.expediente.propiedad = true;

        	this.expediente_show = true;
            this.state.class="";
        },
        return_all() {
            this.state.class="";
        	this.expediente_show = false;
            $('.asesoriaLoad').show();
            if(this.expSearch == "")
                cuAsesoriaLegalStore.methods.loadExpedientes();
            else
                cuAsesoriaLegalStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        registerConformidad(id_expediente) {
            $('.asesoriaLoad').show();
            this.expediente.id_expediente = id_expediente;
            var getConformidad = cuAsesoriaLegalStore.methods.registerConformidad(this.expediente);
            getConformidad.then( (data) => {
                if(data.success){
                    cuAsesoriaLegalStore.methods.loadExpedientes();
                    this.expediente_show = false;
                }
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        }
    },
    created() {
        //rolStore.methods.created();
        this.expediente.propiedad = false;
        cuAsesoriaLegalStore.methods.loadExpedientes();
    }
});