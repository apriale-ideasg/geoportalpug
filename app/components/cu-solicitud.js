Vue.component('cm-solicitud', {
    data() {
        return { 
			cuSolicitudStore: cuSolicitudStore.data,
			expediente_show: false,
            expediente: {},
			expSearch: '',
            state: {
                class: ''
            }
        }
    },
    methods: {
        stateClose(){
            this.state.class="";
        },
        searchExpedientes() { 
        	$('.solicitudLoad').show();
        	cuSolicitudStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        selectExpediente(id_expediente) {
        	$('.solicitudLoad').show();
        	cuSolicitudStore.methods.seleccionarExpediente(id_expediente);
        	this.expediente_show = true;
            this.state.class="";
        },
        return_all() {
            this.state.class="";
        	this.expediente_show = false;	
            $('.asesoriaLoad').show();
            if(this.expSearch == "")
                cuAsesoriaLegalStore.methods.loadExpedientes();
            else
                cuAsesoriaLegalStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        registerSolicitud(id_expediente) {
            //$('.solicitudLoad').show();
            this.expediente.id_expediente = id_expediente;
            var getSolicitud = cuSolicitudStore.methods.registerSolicitud(this.expediente);
            getSolicitud.then( (data) => {
                if(data.success){
                    this.expediente_show = false;
                    cuSolicitudStore.methods.loadExpedientes();
                }
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        }
    },
    created() {
        //rolStore.methods.created();
        cuSolicitudStore.methods.loadExpedientes();
    }
});