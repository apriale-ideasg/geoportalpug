Vue.component('cm-asignar-tecnico', {
    data() {
        return { 
			cuAsignarTecnicoStore: cuAsignarTecnicoStore.data,
			expediente_state: 0,
            tecnico_state: 0,
			expSearch: '',
            expSearchTecnico: '',
            state: {
                class: ''
            }
        }
    },
    methods: {
        stateClose(){
            this.state.class="";
        },
        searchExpedientes() { 
        	$('.asignarTecnLoad').show();
        	cuAsignarTecnicoStore.methods.searchExpedientes(this.expSearch, this.expSearch, this.expSearch);
        },
        searchTecnicos() { 
            $('.asignarTecnLoad').show();
            cuAsignarTecnicoStore.methods.searchTecnicos(this.expSearchTecnico, this.expSearchTecnico);
        },
        selectExpediente(id_expediente) {
        	$('.asignarTecnLoad').show();
        	cuAsignarTecnicoStore.methods.seleccionarExpediente(id_expediente);
        	this.expediente_state = 1;
            this.state.class="";
        },
        selectTecnico(id_tecnico) {
            let id_expediente = this.cuAsignarTecnicoStore.select_expediente.id_expediente;
            let id_asignacion = this.cuAsignarTecnicoStore.select_expediente.id_asignacion;
            
            var getAsignacion = cuAsignarTecnicoStore.methods.selectTecnicoSave(id_tecnico, id_expediente);

            getAsignacion.then( (data) => {
                if(data.success){
                    cuAsignarTecnicoStore.methods.loadExpedientes();
                    this.expediente_state = 1;
                }
                this.state.class="show";
            })
            .catch( e => { this.state.class="show"; })
        },
        return_all() {
            this.state.class="";
        	this.expediente_state = 0;	
        },
        return_exp() {
            this.expediente_state = 1;  
        },
        asignarTecnico(state) {
            $('.asignarTecnLoad').show();
            cuAsignarTecnicoStore.methods.loadTecnicos();
            this.expediente_state = 2;
            this.tecnico_state = state;
        }
    },
    created() {
        //rolStore.methods.created();
        cuAsignarTecnicoStore.methods.loadExpedientes();
    }
});