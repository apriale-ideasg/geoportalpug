Vue.component('cm-fuhu', {
    data() {
        return {
            fuhuStore: fuhuStore.data,
            cuMaestrosStore: cuMaestrosStore.data,
            tramite_check: 1,
            tipo_tramite: [],
            tipo_tramite_opt: [],
            tipo_obra: [],
            tipo_obra_child: [],
            tipo_obra_subchild: [],
            modalidad_aprob: [],
            modalidad_aprob_opt: [],
            anexos: [],
            estado_civil: [],
            tipo_via: [],
            doc_adjuntos: [],

            observaciones: '',
            solicitud : {},
            administrado : {},
            terreno: {
                departamento : "JUNIN",
                provincia : "HUANCAYO",
                distrito : "EL TAMBO"
            },
            proyecto: {},
            proyectistas_cap: [],
            proyectistas_cip: [],
            adjuntos: {
                documento : [],
                doc_val : [],
                doc_desc: []
            },
            message: {
                send: false,
                state: 'success',
                value: ''
            },
            xy_select: false,
            xy_selecting: false
        }
    },
    methods: {
        changeModalidad() {
            const modalidad = this.modalidad_aprob.find( modalidad => modalidad.id_modalidad_aprobacion == this.solicitud.id_modalidad );
            this.modalidad_aprob_opt = modalidad.hijos;
        },
        changeTramite(){
            const tramite = this.tipo_tramite.find( tramite => tramite.id_tipo_tramite == this.solicitud.id_tipo_tramite )
            this.tipo_tramite_opt = tramite.hijos;
            if(tramite.tipo_tramite == "OTROS"){
                this.tramite_check = 0;
            } else { this.tramite_check = 1; }
        },
        changeTipo(){
            const tipo = this.tipo_obra.find( tipo => tipo.id_tipo == this.solicitud.id_tipo )
            this.tipo_obra_child = tipo.hijos;
        },
        changeTipoChild(){
            const tipo = this.tipo_obra_child.find( tipo => tipo.id_tipo == this.solicitud.id_tipo_child )
            this.tipo_obra_subchild = tipo.hijos;
        },
        continueSolicitud(){
            console.log(this.solicitud);
        },
        insertFuhu(){

            $('.fuhuLoad').show();

            let anexos_send = [];
            let documentos = [];

            this.solicitud.anexos.forEach( el => {
                anexos_send.push({"anexo": el});
            })

            this.adjuntos.documento.forEach( (el, index) => {
                documentos.push({"doc": index, "valor": el, "cantidad": this.adjuntos.doc_val[index], "descripcion": this.adjuntos.doc_desc[index] });
            })

            let expediente = {
                "solicitud" : this.solicitud,
                "administrado" : this.administrado,
                "terreno" : this.terreno,
                "adjuntos" : this.adjuntos,
                "proyectoca" : this.proyecto,
                "observaciones" : this.observaciones,
                "proyectistas" : this.proyectistas_cap.concat(this.proyectistas_cip)
            };
            expediente = JSON.parse( JSON.stringify(expediente) );

            //expediente.adjuntos.documento = [];
            expediente.solicitud.anexos = anexos_send;
            expediente.adjuntos.documento = documentos;
            expediente.adjuntos.doc_val = undefined;
            expediente.adjuntos.doc_desc = undefined;

            expediente.proyectistas.forEach( (el, index) => {
                el.id_tipo_proyectista = 1;
                el.especificar = "";
            })

            console.log(expediente);
            //debugger;
            fuhuStore.methods.insertFuhu(expediente).then( (exito, fallo) => {
              if(exito.success == true ){
                  this.message.send = true;
                  this.message.state = 'success';
                  this.message.value = 'El formulario fue registrado exitosamente';
                  this.message.title = '!Felicidades!';
              }
              else if(exito.success == false){
                  this.message.send = true;
                  this.message.state = 'warning';
                  this.message.value = 'Se presentaron errores al registrar el formulario, intentelo nuevamente';
                  this.message.title = 'Ups!';
              }
            });
        },
        returnFuhu(){
          this.message.send = false;
        },
        newFuhu(){

          this.solicitud.id_tipo_tramite = null;

          // this.solicitud.anexos = [];
          // this.administrado.persona = {};
          // this.administrado.domicilio = {};
          // this.administrado.conyuge = {};
          // this.administrado.apoderado = {};
          //
          // this.proyectistas_cap = [{},{}];
          // this.proyectistas_cip = [{},{}];
          //
          // this.observaciones= '';
          // this.solicitud = {};
          // this.administrado = {};
          // this.terreno = {};
          //
          // this.proyecto= {};
          // this.proyectistas_cap= [];
          // this.proyectistas_cip= [];
          // this.adjuntos.documento= [];
          // this.adjuntos.doc_val= [];
          // this.adjuntos.doc_desc= [];
        },
        searchxy(){
            removeInteractions();
            drawHouse('Point');
            this.xy_selecting = true;
            //this.xy_select = true;
            terrenoMap = true;
        },
        searchxy_out(){
            removeInteractions();

            this.terreno.lat = $('.lat-map-disabled')[0].value;
            this.terreno.long = $('.lon-map-disabled')[0].value;

            this.xy_selecting = false;
            this.xy_select = true;
            terrenoMap = false;
        }
    },
    computed: {

    },
    created() {
        var getTipoTramite = fuhuStore.methods.getTipoTramite();
        getTipoTramite.then( () => {
            this.tipo_tramite = checkNivel(0, null, this.fuhuStore.tipo_tramite);
        })

        var getTipoObra = fuhuStore.methods.getTipoObra();
        getTipoObra.then( () => {
            this.tipo_obra = checkNivel(0, null, this.fuhuStore.tipo_obra);
        })

        var getModalidadAprobacion = fuhuStore.methods.getModalidadAprobacion();
        getModalidadAprobacion.then( () => {
            this.modalidad_aprob = checkNivel(0, null, this.fuhuStore.modalidad_aprobacion);
        })

        fuhuStore.methods.getAdjuntos().then( () => {
            this.doc_adjuntos = this.fuhuStore.doc_adjuntos;
        });

        fuhuStore.methods.getAnexos().then( () => {
            this.anexos = this.fuhuStore.anexos;
        });

        cuMaestrosStore.methods.getDepartamento().then();
        cuMaestrosStore.methods.getProvincias().then();
        cuMaestrosStore.methods.getDistritos().then();
        cuMaestrosStore.methods.getEstadoCivil().then();
        cuMaestrosStore.methods.getTipoVia();

        this.solicitud.anexos = [];
        this.administrado.persona = {};
        this.administrado.domicilio = {};
        this.administrado.conyuge = {};
        this.administrado.apoderado = {};

        this.proyectistas_cap = [{},{}];
        this.proyectistas_cip = [{},{}];
    }
});
