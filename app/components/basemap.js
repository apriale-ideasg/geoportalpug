Vue.component('cm-basemap', {
    data() {
        return {
            basemapStore: [
                { id: 1, name: "OpenStreet Map" },
                { id: 2, name: "Bing Map" },
                { id: 3, name: "Bing Aerial Map" },
            ], 
            selectBase: 1
        }
    },
    methods: {
        changeBase() {
            switch (this.selectBase) {
                case 1:
                    layerVisible();
                    osm.setVisible(true);
                    break;
                case 2:
                    layerVisible();
                    bing.setVisible(true);
                    break;
                case 3:
                    layerVisible();
                    bingAerial.setVisible(true);
                    break;
                default:
                    osm.setVisible(true);
                    break;
            }
        }
    },
    created() {
        $('.baseMapLoad').hide();
    }
});

function layerVisible() {
    osm.setVisible(false);
    bing.setVisible(false);
    bingAerial.setVisible(false);
}