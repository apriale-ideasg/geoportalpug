//import rolStore from "../stores/rolStore"

new Vue({
    el: '#app',
    data: {
        loginStore: loginStore.data,
        username: '',
        userpassword: '123',
        loginStatus: ''
    },
    methods: {
        login() {
            //loginStore.data.userLogged = true;

            this.loginStatus = "";
            axios.get(host+`/log/login?userId=` + this.username + "&password=" + this.userpassword)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.Result == "OK") {
                        if(response.data.data != null){
                            loginStore.data.user.id_personal = response.data.data.id_personal;
                            loginStore.data.user.id_usuario = response.data.data.id_usuario;
                            loginStore.data.user.tipo_usuario = response.data.data.tipo_usuario;
                            loginStore.data.user.usuario = response.data.data.usuario;
                            loginStore.data.userLogged = true;
                        }
                        else{
                            this.loginStatus = "Usuario incorrecto";
                        }
                    }
                })
                .catch(e => {
                    this.loginStatus = "Error al conectarse con el servidor";
                })
        },
        logout() {
            //loginStore.data.userLogged = false;
            location.reload();
        }
    }
})