$(function() {
    "use strict";
    $(function() {
        $(".preloader").fadeOut();
    });
    jQuery(document).on('click', '.mega-dropdown', function(e) {
        e.stopPropagation()
    });
    // ============================================================== 
    // This is for the top header part and sidebar part
    // ==============================================================  

    var set = function() {
        var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        var topOffset = 70;
        if (width < 500) {
            $("body").addClass("mini-sidebar");
            $('.navbar-brand span').hide();
            $(".scroll-sidebar, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible");
            $(".sidebartoggler i").addClass("ti-menu");
        } else {
            $("body").removeClass("mini-sidebar");
            $('.navbar-brand span').show();
            $(".sidebartoggler i").removeClass("ti-menu");
        }

        var height = ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $(".page-wrapper").css("min-height", (height) + "px");
        }

    };
    $(window).ready(set);
    $(window).on("resize", set);

    // topbar stickey on scroll

    $(".fix-header .topbar").stick_in_parent({

    });

    // this is for close icon when navigation open in mobile view
    $(".nav-toggler").click(function() {
        $("body").toggleClass("show-sidebar");
        $(".nav-toggler i").toggleClass("ti-menu");
        $(".nav-toggler i").addClass("ti-close");
    });
    $(".sidebartoggler").on('click', function() {
        $(".sidebartoggler i").toggleClass("ti-menu");
    });

    // ============================================================== 
    // Auto select left navbar
    // ============================================================== 
    $(function() {
        var url = window.location;
        var element = $('ul#sidebarnav a').filter(function() {
            return this.href == url;
        }).addClass('active').parent().addClass('active');
        while (true) {
            if (element.is('li')) {
                element = element.parent().addClass('in').parent().addClass('active');
            } else {
                break;
            }
        }
    });



    $('#control-layer-btn').on('click', function(ev){
        $('.list-menu, .menu-options>div').hide();
        $('.menu-options').slideDown();
        ev.preventDefault();
    });

    $('#search-spacial-btn').on('click', function(ev){
        $('.list-menu, .menu-options>div').hide();
        $('.search-spacial').show();
        $('.menu-options').slideToggle();
        ev.preventDefault();
    });

    $('#search-titular-btn').on('click', function(ev){
        $('.list-menu, .menu-options>div').hide();
        $('.search-titular').show();
        $('.menu-options').slideToggle();
        ev.preventDefault();
    });

    $('.btn-return-menu').on('click', function(ev){
        $('.list-menu').slideToggle();
        $('.menu-options').hide();
        ev.preventDefault();
    });
    
    $('#base-map-btn').on('click', function(ev){
        $('.list-menu, .menu-options>div').hide();
        $('.base-map').show();
        $('.menu-options').slideToggle();
        ev.preventDefault();
    })
    $('#external-layer-btn').on('click', function(ev){
        $('.list-menu, .menu-options>div').hide();
        $('.external-layer').show();
        $('.menu-options').slideToggle();
        ev.preventDefault();
    })

    // ============================================================== 
    // Modal
    // ============================================================== 
    $('#nav-acotar').on('click', function() {
        modalShow("#modalAcotar");
    })
    $('#nav-solicitud').on('click', function() {
        modalShow("#modalSolicitud");
    })
    $('#nav-ases-legal').on('click', function () {
        modalShow("#modalAsesoriaLegal");
    })
    $('#nav-asig-tecn').on('click', function() {
        modalShow("#modalAsignarTecnico");
    })
    $('#nav-expedientes').on('click', function() {
        modalShow("#modalExpedientes");
    })
    $('#nav-liquidacion').on('click', function () {
        modalShow("#modalLiquidacion");
    })
    $('#nav-resolucion').on('click', function () {
        modalShow("#modalResolucion");
    })

    $('#nav-privilege').on('click', function() {
        modalShow("#modalPrivilege");
    })
    $('#nav-legend').on('click', function() {
        modalShow("#modalLegend");
    })
    $('#nav-configLayer').on('click', function () {
        modalShow("#modalConfigLayer");
    })

    $('#nav-rol').on('click', function () {
        modalShow("#modalRol");
    })

    $('#nav-basemap').on('click', function () {
        modalShow("#modalBaseMap");
    })

    $('#nav-fue').on('click', function(){
        modalShow("#modalFue");
        //$('.nav-fue').scrollingTabs();
    })
    $('#nav-fuhu').on('click', function(){
        modalShow("#modalFuhu");
        $('.nav-fuhu').scrollingTabs();
    })


    $('#nav-externalLayer').on('click', function () {
        modalShow("#modalExternalLayer");
    })

    $('#nav-busquedaEsp').on('click', function () {
        modalShow("#modalSearchSpacial");
    })

    $('#nav-busquedaTit').on('click', function () {
        modalShow("#modalSearchTitular");
    })

    $('#idCoor').on('click', function () {
        modalShow("#modalCoordenadas");
    }) 

    $('#nav-contacto').on('click', function () {
        modalShow("#modalContacto");
    })

    $('#nav-wmskml').on('click', function () {
        modalShow("#modalWMSKML");
    })
    
    $("#app").on('click', '.modal .close[data-dismiss="modal"]',function(ev) {
        $(this).parents('.modal').removeClass("modal-show");
    })
    
    $('#app').on('click', '.modal', function (ev) {
        $('.modal').css("z-index", 1050);
        $(this).css("z-index",1051);
    })

    function menuHide(){
        if ($('body').hasClass("show-sidebar")) {
            $('body').removeClass("show-sidebar");
            $('.ti-close').addClass("ti-menu")
        }
    }
    
    function modalShow(modal) {
        $(modal).addClass("modal-show");
        $(modal).draggable({
            handle: ".modal-header"
        });
        menuHide();
    }
    // ============================================================== 
    // Slimscrollbars
    // ============================================================== 
    $('.scroll-sidebar').slimScroll({
        position: 'left',
        size: "5px",
        height: '100%',
        color: '#dcdcdc'
    });

    // ============================================================== 
    // Resize all elements
    // ============================================================== 
    $("body").trigger("resize");

    $('.pop-up-close').on('click', function() {
        $(this).parent().hide();
    })

    // ============================================================== 
    // Scrolling tabs
    // ============================================================== 
    //$('.nav-tabs').scrollingTabs();

    $('#app').on('click', '.btn-visualizar', function (ev) {
        var id_expediente = this.getAttribute('data-visualizar');
        var expediente = fuStore.methods.loadExpediente(id_expediente);

        expediente.then( () => {
            var tipo = fuStore.data.expediente.id_tipo_formulario;
            if(tipo == "1"){
                fueStore.data.show_id_expediente = id_expediente;
                fueStore.methods.buscarExpediente();

                $('#modalFueShow').addClass("modal-show");
                $('#modalFueShow').draggable({
                    handle: ".modal-header"
                });
                $('.nav-fue-show').scrollingTabs();
            }
            else if(tipo == "2"){
                fuhuStore.data.show_id_expediente = id_expediente;
                fuhuStore.methods.buscarExpediente();

                $('#modalFuhuShow').addClass("modal-show");
                $('#modalFuhuShow').draggable({
                    handle: ".modal-header"
                });
                $('.nav-fuhu-show').scrollingTabs();
            }
        }, () => {
            alert("No se pudo obtener el expediente deseado");
        })
    })

});

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
//import Vue from 'vue'
//import VueLayers from 'vuelayers'
//import App from './App'
//import Customers from './components/Customers'
//import About from './components/About'
//import header from './components/header'
//Vue.use(VueLayers)
/* eslint-disable no-new */
