const cuSolicitudStore = {
    data: {
        expedientes: [],
        select_expediente: [],
        errors: []
    },
    methods: {
    	loadExpedientes() {
            axios.get(host+'/formulario/solicitud')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuSolicitudStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuSolicitudStore.data.expedientes.push(element);
                    });

                    $('.solicitudLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        searchExpedientes(num_doc, nombap, n_exp) {
            axios.get(host+'/formulario/solicitud/b_expediente?num_doc='+num_doc+'&appnom='+nombap+'&n_exp='+n_exp)
            //axios.get(host_test+'/expedientes_s.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuSolicitudStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuSolicitudStore.data.expedientes.push(element);
                    });

                    $('.solicitudLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        seleccionarExpediente(id_expediente) {
            axios.get(host+'/formulario/solicitud/expediente?id_expediente='+id_expediente)
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data.length > 0) {
                    	cuSolicitudStore.data.select_expediente = response.data.data[0];
                    }

                    $('.solicitudLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        registerSolicitud() {
            var registerSolicitudPromise = new Promise((resolve, reject) => {
                axios.post(host+`/formulario/solicitud/update`, cuSolicitudStore.data.select_expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Solicitud actualizada con exito");
                        $('.solicitudLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuSolicitudStore.data.errors.push(e);
                    reject(e);
                    console.warn(e);
                })
                $('.solicitudLoad').show();
            })
            return registerSolicitudPromise;
        }
    }
};