//import axios from 'axios';

const fueStore = {
	data: {
		tipo_tramite: [],
		tipo_obra: [],
		modalidad_aprobacion: [],
		anexos: [],
		doc_adjuntos: [],
		edificacion: [],

		proyecto : {
            pisos : []
        },
		errors: [],
		show_id_expediente: '',
		expediente: {
			base : {},
			solicitud: {
				tipo: [],
				tipo_tramite: [],
				modalidad_aprobacion: [],
				anexos: []
			},
			propietario: {
				persona: {},
				domicilio: {},
				conyuge: {},
				propietario: {},
				empresa: {}
			},
			requisitos: [],
			terreno: {},
			proyecto: {
				proyectista: {},
				area: {},
				pisos: []
			},
			valor_obra: [],
			edificacion: {
				detalle_edificacion: [],
				concepto: {}
			}
		}
	},
	methods: {
		getTipoTramite(){
			var tipoTramitePromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/tipo-tramite?id_tipo_formulario='+ '1')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fueStore.data.tipo_tramite.push(element);
						});
						$('.fueLoad').hide();
						resolve(); // fulfilled
						//rolStore.data.rols.push(response.data.data);
					})
					.catch(e => {
						fueStore.data.errors.push(e);
						reject();
					})
			})
			return tipoTramitePromise;
		},
		getTipoObra(){
			var tipoObraPromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/tipo?id_tipo_formulario='+ '1')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fueStore.data.tipo_obra.push(element);
						});
						$('.fueLoad').hide();
						resolve(); // fulfilled
						//rolStore.data.rols.push(response.data.data);
					})
					.catch(e => {
						fueStore.data.errors.push(e);
						reject();
					})
			})
			return tipoObraPromise;
		},
		getModalidadAprobacion(){
			var modalidadAprobPromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/modalidad-aprobacion')
					.then(response => {
						// JSON responses are automatically parsed.
						fueStore.data.modalidad_aprobacion = [];
						response.data.data.forEach(element => {
							fueStore.data.modalidad_aprobacion.push(element);
						});
						$('.fueLoad').hide();
						resolve(); // fulfilled
						//rolStore.data.rols.push(response.data.data);
					})
					.catch(e => {
						fueStore.data.errors.push(e);
						reject();
					})
			})
			return modalidadAprobPromise;
		},
		getAnexos(){
			var anexosPromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/anexos?id_tipo_formulario='+ '1')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fueStore.data.anexos.push(element);
						});
						$('.fueLoad').hide();
						resolve(); // fulfilled
						//rolStore.data.rols.push(response.data.data);
					})
					.catch(e => {
						fueStore.data.errors.push(e);
						reject();
					})
			})
			return anexosPromise;
		},
		getAdjuntos(){
			var adjuntosPromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/documentos-adjuntos?id_tipo_formulario='+ '1')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fueStore.data.doc_adjuntos.push(element);
						});
						$('.fueLoad').hide();
						resolve(); // fulfilled
					})
					.catch(e => {
						fueStore.data.errors.push(e);
						reject();
					})
			})
			return adjuntosPromise;
		},
		getEdificacion(){
			var edificacionPromise = new Promise((resolve, reject) => {
				axios.get(host_test+'/fue-edificacion.json?id_tipo_formulario='+ '1')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fueStore.data.edificacion.push(element);
						});
						$('.fueLoad').hide();
						resolve(); // fulfilled
					})
					.catch(e => {
						fueStore.data.errors.push(e);
						reject();
					})
			})
			return edificacionPromise;
		},
		insertFue(expediente){
			var insertPromise = new Promise((resolve, reject) => {
				axios.post(host+`/formulario/insert-test`, expediente)
	            .then(response => {
	                // JSON responses are automatically parsed.
	                $('.fueLoad').hide();
	                resolve(response.data);
	                if (response.data.Result == "OK") {
	                    console.log("Capa guardada con exito");
	                }
	                //$('.configLLoad').hide();
	            })
	            .catch(e => {
	                this.errors.push(e)
	            	console.log(this.errors);
	            })
	        })
			return insertPromise;
		},
		buscarExpediente(){
			var insertPromise = new Promise((resolve, reject) => {
				var id_expediente = fueStore.data.show_id_expediente;

				axios.get(host+`/expediente/expediente?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.total > 0){
		                fueStore.data.expediente.base = response.data.data[0];
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

				axios.get(host+`/expediente/solicitud?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success){
		                fueStore.data.expediente.solicitud.tipo = response.data.tipo;
		            	fueStore.data.expediente.solicitud.anexos = response.data.anexos;
		            	fueStore.data.expediente.solicitud.tipo_tramite = response.data.tipo_tramite;
		            	fueStore.data.expediente.solicitud.modalidad_aprobacion = response.data.modalidadaprobacion;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/propietario?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success){
		                fueStore.data.expediente.propietario.persona = response.data.persona;
		            	fueStore.data.expediente.propietario.domicilio = response.data.domicilio;
		            	fueStore.data.expediente.propietario.conyuge = response.data.conyuge;
		            	fueStore.data.expediente.propietario.propietario = response.data.propietario[0];
		            	fueStore.data.expediente.propietario.empresa = response.data.empresa;
		            	fueStore.data.expediente.propietario.apoderado = response.data.apoderado;
		            	fueStore.data.expediente.propietario.apoderado_domicilio = response.data.apoderado_domicilio;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/docajuntos?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success){
		                fueStore.data.expediente.requisitos = response.data.data;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })
	            
	            axios.get(host+`/expediente/terreno?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.total > 0){
		                fueStore.data.expediente.terreno = response.data.data[0];
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/fueproyecto?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.proyecto.length > 0){
		                fueStore.data.expediente.proyecto.area = response.data.proyecto[0];
		                fueStore.data.expediente.proyecto.pisos = response.data.pisos;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/valorobra?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.total > 0){
		                fueStore.data.expediente.valor_obra = response.data.data;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/edificacion?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.edificacion.length > 0){
		                fueStore.data.expediente.edificacion.concepto = response.data.edificacion[0];
		                fueStore.data.expediente.edificacion.detalle_edificacion = response.data.detalleEdificacion;
		                if(response.data.regimen != undefined)
		                	fueStore.data.expediente.edificacion.concepto.regimen_propiedad = response.data.regimen.regimen_propiedad;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/proyectista?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.total > 0){
		                fueStore.data.expediente.proyecto.proyectista = response.data.data;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })
	            
            })
			return insertPromise;
		}
	}
};


//export default rolStore;
