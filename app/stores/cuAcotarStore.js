const cuAcotarStore = {
    data: {
        expedientes: [],
        select_expediente: [],
        errors: []
    },
    methods: {
    	loadExpedientes() {
            axios.get(host+'/formulario/acotar')
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuAcotarStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuAcotarStore.data.expedientes.push(element);
                    });

                    $('.acotarLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        searchExpedientes(num_doc, nombap, n_exp) {
            axios.get(host+'/formulario/acotar/b_expediente?num_doc='+num_doc+'&appnom='+nombap+'&n_exp='+n_exp)
            //axios.get(host_test+'/expedientes_s.json')        
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuAcotarStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuAcotarStore.data.expedientes.push(element);
                    });

                    $('.acotarLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        seleccionarExpediente(id_expediente) {
            axios.get(host+'/formulario/acotar/expediente?id_expediente='+id_expediente)
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data.length > 0) {
                    	cuAcotarStore.data.select_expediente = response.data.data[0];
                    }

                    $('.acotarLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
    }
};