const loginStore = {
    data: {
        user: {
            id_personal :"",
            id_usuario :"",
            tipo_usuario:"",
            usuario:""
        },
        userLogged: false,
        errors: []
    },
    methods: {
        login(){
            loginStore.data.userLogged = true;
        }
    }
};