//import axios from 'axios';

const privilegioStore = {
    data: {
        privilegios: [],
        errors: []
    },
    methods: {
        savePrivilegioRol(insert, rolname){
            privilegioStore.data.privilegios.forEach(element => {
                axios.get(host+`/roles/privilegios/revoke/?tabla=` + element.tabla + `&rolname=` + rolname)
                    .then(response => {
                        // JSON responses are automatically parsed.
                        if(response.data.Result == "OK"){
                            console.log("Se eliminaron exitosamente");
                        }
                    })
                    .catch(e => {
                        privilegioStore.data.errors.push(e)
                    })
            });
            insert.forEach(element => {
                axios.get(host+`/roles/privilegios/create?privilege=` + element.privilege + `&tabla=` + element.tabla + `&rolname=` + rolname)
                    .then(response => {
                        // JSON responses are automatically parsed.
                        if (response.data.Result == "OK") {
                            console.log("Se guardaron exitosamente");
                        }
                        $('.privilegioLoad').hide();
                    })
                    .catch(e => {
                        privilegioStore.data.errors.push(e)
                    })
            });
        },
        created() {
            axios.get(host+`/roles/tablas`)
                .then(response => {
                    // JSON responses are automatically parsed.
                    response.data.data.forEach(element => {
                        privilegioStore.data.privilegios.push(element);
                    });
                    $('.privilegioLoad').hide();
                })
                .catch(e => {
                    privilegioStore.data.errors.push(e)
                })
        },
        privilegioRol(rol) {
            axios.get(host+`/roles/privilegios?rol=`+rol)
                .then(response => {
                    // JSON responses are automatically parsed.
                    var privilegioRol = [];
                    response.data.data.forEach(element => {
                        privilegioRol.push(element);
                    });
                    privilegioStore.data.privilegios = privilegioRol;
                    $('.privilegioLoad').hide();
                })
                .catch(e => {
                    privilegioStore.data.errors.push(e);
                })
        }
    }
};

//export default privilegioStore;