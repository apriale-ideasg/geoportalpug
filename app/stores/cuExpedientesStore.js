const cuExpedientesStore = {
    data: {
        expedientes: [],
        select_expediente: [],
        verificacion: [],
        errors: [],
        observaciones: [],
        doc_presentados: [],
        antecedentes: [],
        requisitos: [],
        parametros_urb: {},
        parametros_edif: {},
        dcAreas: []
    },
    methods: {
    	loadExpedientes() {
            axios.get(host+'/formulario/controltecnico?id_tecnico='+loginStore.data.user.id_personal)
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuExpedientesStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.expedientes.push(element);
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        searchExpedientes(num_doc, nombap, n_exp) {
            axios.get(host+'/formulario/controltecnico/b_expediente?num_doc='+num_doc+'&appnom='+nombap+'&nro_expediente='+n_exp+'&id_tecnico='+1)
            //axios.get(host_test+'/expedientes_s.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuExpedientesStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.expedientes.push(element);
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        seleccionarExpediente(id_expediente) {
            axios.get(host+'/formulario/controltecnico/expediente?id_expediente='+id_expediente)
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data.length > 0) {
                    	cuExpedientesStore.data.select_expediente = response.data.data[0];
                    }

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        seleccionarVerificacion(id_expediente) {
            //axios.get(host_test+'/verificacion.json')
            axios.get(host+'/informe/verificacion?id_expediente='+id_expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuExpedientesStore.data.verificacion = [];

                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.verificacion.push(element);
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e)
                })
        },
        registerVerificacion(id_expediente){
            var verifPromise = new Promise((resolve, reject) => {
                var verificacionads = JSON.parse( JSON.stringify(cuExpedientesStore.data.verificacion) );
                verificacionads.forEach( el => {
                    el.descripcion = undefined;
                    el.observaciones = el.observaciones.replace(",",".","gm")
                })
                var verificacion = {
                    'id_expediente': id_expediente,
                    'verificacionads':verificacionads
                }
                axios.post(host+`/informe/verificacion/insert`, verificacion)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Verificacion creado con exito");
                        $('.tecnicoLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e);
                    reject(e);
                    console.log(this.errors);
                })
            })
            return verifPromise;
        },
        registerInforme(informe) {
            var infPromise = new Promise((resolve, reject) => {
                axios.post(host+`/formulario/informe/insert`, informe)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Informe creado con exito");
                        $('.tecnicoLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    this.errors.push(e)
                    console.log(this.errors);
                })
            })
            return infPromise;
        },
        registerProgramacion(expediente) {
            expediente.id_expediente = cuExpedientesStore.data.select_expediente.id_expediente;
            var programacionPromise = new Promise((resolve, reject) => {
                axios.post(host+`/formulario/programar/update`, expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Programación actualizada con exito");
                        $('.tecnicoLoad').hide();
                    }
                })
                .catch(e => {
                    this.errors.push(e)
                    console.log(this.errors);
                })
            })
            return programacionPromise;
        },
        registerObservaciones(observaciones, id_expediente) {

            var obsPromise = new Promise((resolve, reject) => {
                axios.post(host+`/formulario/observacion/insert`, {'observaciones': observaciones, 'id_expediente' : id_expediente})
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("observaciones insertadas con exito");
                        $('.tecnicoLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e);
                    reject(e);
                    console.log(this.errors);
                })
            })
            return obsPromise;
        },
        loadObservaciones(id_expediente) {
            //axios.get(host+'/formulario/controltecnico?id_tecnico='+loginStore.data.user.id_personal)
            //axios.get(host_test+'/observacion.json')
            axios.get(host+'/formulario/observacion?id_expediente='+id_expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuExpedientesStore.data.observaciones = [];

                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.observaciones.push(element);
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e)
                })
        },
        registerAntecedente(antecedentes, id_expediente) {
            var antPromise = new Promise((resolve, reject) => {
                axios.post(host+`/informe/antecedentes/insert`, {'antecedentes': antecedentes, 'id_expediente' : id_expediente})
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("antecedentes insertadas con exito");
                        $('.tecnicoLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e);
                    reject(e);
                    console.log(this.errors);
                })
            })
            return antPromise;
        },
        loadAntecedentes(id_expediente) {
            axios.get(host+'/informe/antecedentes?id_expediente='+id_expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuExpedientesStore.data.antecedentes = [];

                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.antecedentes.push(element);
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e)
                })
        },
        registerDocPresentados(docs, id_expediente) {

            var docsPromise = new Promise((resolve, reject) => {
                axios.post(host+`/informe/documentos/insert`, {'docs': docs, 'id_expediente' : id_expediente})
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("docs insertados con exito");
                        $('.tecnicoLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e);
                    reject(e);
                    console.log(this.errors);
                })
            })
            return docsPromise;
        },
        loadDocPresentados(id_expediente) {
            //axios.get(host+'/formulario/controltecnico?id_tecnico='+loginStore.data.user.id_personal)
            //axios.get(host_test+'/observacion.json')
            axios.get(host+'/informe/documentos?id_expediente='+id_expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuExpedientesStore.data.doc_presentados = [];

                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.doc_presentados.push(element);
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e)
                })
        },
        registerDocPresentados(docs, id_expediente) {

            var docsPromise = new Promise((resolve, reject) => {
                axios.post(host+`/informe/documentos/insert`, {'docs': docs, 'id_expediente' : id_expediente})
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("docs insertados con exito");
                        $('.tecnicoLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e);
                    reject(e);
                    console.log(this.errors);
                })
            })
            return docsPromise;
        },
        loadParametros(id_expediente) {
            axios.get(host+'/informe/paramproy?id_expediente='+id_expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.parametros_edif = element;
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e)
                })

            axios.get(host+'/informe/paramurb?id_expediente='+id_expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.parametros_urb = element;
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e)
                })
        },
        registerParametros(id_expediente, param_urb, param_proy) {
            var parmPromise = new Promise((resolve, reject) => {
                axios.post(host+`/informe/parametros/insert`, {'param_urb': param_urb, 'param_proy': param_proy, 'id_expediente' : id_expediente})
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("params insertados con exito");
                        $('.tecnicoLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e);
                    reject(e);
                    console.log(this.errors);
                })
            })
            return parmPromise;
        },
        loadDcAreas(id_expediente) {
            axios.get(host+'/informe/verifdcareas?id_expediente='+id_expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuExpedientesStore.data.dcAreas = [];
                    response.data.data.forEach(element => {
                        cuExpedientesStore.data.dcAreas.push(element);
                    });

                    $('.tecnicoLoad').hide();
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e)
                })
        },
        registerDcAreas(id_expediente, pisos) {
            var pisosPromise = new Promise((resolve, reject) => {
                axios.post(host+`/informe/verifdcareas/insert`, {'pisos': pisos, 'id_expediente' : id_expediente})
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("pisos insertados con exito");
                        $('.tecnicoLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuExpedientesStore.data.errors.push(e);
                    reject(e);
                    console.log(this.errors);
                })
            })
            return pisosPromise;
        },
    }
};