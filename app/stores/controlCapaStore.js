const controlCapaStore = {
    data: {
        capas: [],
        capaExternal : [],
        capaVisible: [],
        capaMap: [],
        grupos: [],
        errors: []
    },
    methods: {
        showList(rol) {
            controlCapaStore.data.capas = []; controlCapaStore.data.grupos = []; controlCapaStore.data.capaVisible= [];

            var mdtlayers = {
                    data : [
                        {
                            id_capa: "1",
                            tabla_capa: "sp_formularios",
                            nomb_capa: "Formularios",
                            tg_config_grupo: {
                                id_grupo: "2",
                                grupo: "Control urbano"
                            }
                        },
                        {
                            id_capa: "1",
                            tabla_capa: "tbl_coordenadas_fiscalizacion",
                            nomb_capa: "Fiscalización",
                            tg_config_grupo: {
                                id_grupo: "3",
                                grupo: "Fiscalizacion"
                            }
                        },
                        {
                            id_capa: "2",
                            tabla_capa: "sp_sector",
                            nomb_capa: "Sectores",
                            tg_config_grupo: {
                                id_grupo: "1",
                                grupo: "Catastro"
                            }
                        },
                        {
                            id_capa: "3",
                            tabla_capa: "sp_manzana",
                            nomb_capa: "Manzanas",
                            tg_config_grupo: {
                                id_grupo: "1",
                                grupo: "Catastro"
                            }
                        },
                        {
                            id_capa: "4",
                            tabla_capa: "sp_lote",
                            nomb_capa: "Lotes",
                            tg_config_grupo: {
                                id_grupo: "1",
                                grupo: "Catastro"
                            }
                        }
                    ]
                };

            mdtlayers.data.forEach(element => {
                    element.checked = true;

                    var capa = {};
                    capa.id_capa = element.id_capa;
                    capa.tabla_capa = element.tabla_capa;
                    capa.nomb_capa = element.nomb_capa;
                    capa.id_grupo = element.tg_config_grupo.id_grupo;
                    capa.grupo = element.tg_config_grupo.grupo;
                    capa.checked = true;
                    controlCapaStore.data.capas.push(capa);
                    controlCapaStore.data.capaVisible.push({ 'nomb_capa': element.nomb_capa, 'tabla_capa': element.tabla_capa, 'visible': true});

                    var limiteExist = false;
                    controlCapaStore.data.grupos.forEach(limit => {
                        if (limit.id_grupo == element.tg_config_grupo.id_grupo)
                            limiteExist = true;
                    });
                    if(!limiteExist){
                        var new_grupo = {};
                        new_grupo.id_grupo = element.tg_config_grupo.id_grupo;
                        new_grupo.grupo = element.tg_config_grupo.grupo;
                        controlCapaStore.data.grupos.push(new_grupo);
                    }

                });

                    var format = 'image/png';
                    var mSRS = "EPSG:900913";
                    var nameLyr = ["tematico_1", "tematico_2"];

                    controlCapaStore.data.capas.forEach(element => {
                        var la = new ol.layer.Image({
                            source: new ol.source.ImageWMS({
                                url: geoserver+'/' + workSpace + '/wms',
                                params: { 'LAYERS': workSpace + ':' + element.tabla_capa, 'SRS': mSRS },
                                serverType: 'geoserver',
                                crossOrigin: 'anonymous',
                                // Countries have transparency, so do not fade tiles:
                                transition: 0
                            }),
                            opacity: 0.7
                        });
                        
                        capaMapStore.data.imp += workSpace + ":"+element.tabla_capa+",";

                        map.addLayer(la);
                        
                        if (element.tabla_capa == "sp_lote") {
                            var source = new ol.source.ImageWMS({
                                url: geoserver+'/' + workSpace + '/wms',
                                params: { 'LAYERS': workSpace + ':' + element.tabla_capa, 'SRS': mSRS },
                                serverType: 'geoserver',
                                // Countries have transparency, so do not fade tiles:
                                transition: 0
                            });
                            
                            map.on('singleclick', function (evt) {
                                $('#popup, #popup-fiscalizacion, #popup-expediente').hide();
                                var view = map.getView();
                                var viewResolution = view.getResolution();
                                var url = source.getGetFeatureInfoUrl(
                                    evt.coordinate, viewResolution, 'EPSG:900913',
                                    { 'INFO_FORMAT': 'application/vnd.ogc.gml', 'LAYERS': workSpace + ':sp_lote', 'FEATURE_COUNT': 10 }
                                );
                                if (url) {
                                    var id_lote = "";
                                    var xhttp = new XMLHttpRequest();
                                    xhttp.onreadystatechange = function () {
                                        if (this.readyState == 4 && this.status == 200) {
                                            var parser = new DOMParser();
                                            var xmlDoc = parser.parseFromString(this.response, "text/xml");
                                            
                                            if (xmlDoc.getElementsByTagName(workSpace + ":sp_lote")[0] != undefined){
                                                var x = xmlDoc.getElementsByTagName(workSpace + ":sp_lote")[0].getAttribute("fid");
                                                id_lote = x.substr(8);
                                                $('#popup').show();
                                                $('#popup-idLote').text(id_lote);
                                                
                                                //axios.get(host+"/unidadcatlote?id="+id_lote) 03045032
                                                axios.get(host+"/catastro/unidadcatlote?id="+id_lote)
                                                    .then(response => {
                                                        if(response.data.total == 0){
                                                            $('#popup-error-info').show();
                                                        }
                                                        else{
                                                            axios.get(host+"/loteinfo?uc="+response.data.data[0].cod_lote)
                                                            .then(response => {
                                                                if(response.data.total == 0){
                                                                    $('#popup-error-info').show();
                                                                }
                                                                else{
                                                                    $('#popup-content ul').show();
                                                                    $('#popup-area').text(response.data.data[0].area_terreno);
                                                                    $('#popup-frente').text(response.data.data[0].longitud_frente);
                                                                    $('#popup-direccion').text(response.data.data[0].numero_direccion);
                                                                    $('#popup-predio').text(response.data.data[0].predio_id);
                                                                    $('#popup-uso').text(response.data.data[0].uso);
                                                                    $('#popup-persona').text(response.data.data[0].propietario);
                                                                }
                                                            })
                                                            .catch(e => {
                                                                console.log("Error al intentar traer la información del lote: "+e)
                                                            })
                                                        }
                                                })
                                                .catch(e => {
                                                    console.log("Error al intentar traer la información del lote: "+e)
                                                })
                                                axios.get(host+"/catastro/foto?id=" + id_lote)
                                                    .then(response => {
                                                        if (response.data.total == 0) {
                                                            $('#popup-error-foto').show();
                                                        }
                                                        else {
                                                            $('#popup-foto').show();
                                                            $('#popup-foto').attr("src", "data:image/jpeg;base64,"+response.data.data[0].img);
                                                        }
                                                    })
                                                    .catch(e => {
                                                        console.log("Error al intentar traer la foto del lote: " + e)
                                                    })
                                            }
                                        }
                                    };
                                    xhttp.open("GET", url, true);
                                    xhttp.send();

                                    
                                    var coordinate = evt.coordinate;
                                    popup.setPosition(coordinate);
                                }
                            })
                        } else if (element.tabla_capa == "tbl_coordenadas_fiscalizacion") {
                            var source = new ol.source.ImageWMS({
                                url: geoserver+'/' + workSpace + '/wms',
                                params: { 'LAYERS': workSpace + ':' + element.tabla_capa, 'SRS': mSRS },
                                serverType: 'geoserver',
                                // Countries have transparency, so do not fade tiles:
                                transition: 0
                            });
                            
                            map.on('singleclick', function (evt) {
                                $('#popup, #popup-expediente, #popup-lote').hide();
                                var view = map.getView();
                                var viewResolution = view.getResolution();
                                var url = source.getGetFeatureInfoUrl(
                                    evt.coordinate, viewResolution, 'EPSG:900913',
                                    { 'INFO_FORMAT': 'application/vnd.ogc.gml', 'LAYERS': workSpace + ':tbl_coordenadas_fiscalizacion', 'FEATURE_COUNT': 10 }
                                );
                                if (url) {
                                    var idprediourbano = "";
                                    var xhttp = new XMLHttpRequest();
                                    xhttp.onreadystatechange = function () {
                                        if (this.readyState == 4 && this.status == 200) {
                                            var parser = new DOMParser();
                                            var xmlDoc = parser.parseFromString(this.response, "text/xml");
                                            if (xmlDoc.getElementsByTagName(workSpace + ":tbl_coordenadas_fiscalizacion")[0] != undefined){
                                                var x = xmlDoc.getElementsByTagName(workSpace + ":tbl_coordenadas_fiscalizacion")[0].getAttribute("fid");
                                                idprediourbano = x.substr(30);
                                                $('#popup').show();
                                                $('#popup-idFisc').text(idprediourbano);
                                                
                                                //axios.get(host+"/unidadcatlote?id="+id_lote) 03045032
                                                axios.get(host+"/movile/sentGeoportal/foto?idprediourbano="+idprediourbano)
                                                    .then(response => {
                                                        if(response.data.total == 1){
                                                            var predio = response.data.data[0];
                                                            $('#popup-fiscalizacion').show();
                                                            $('#popup-dniFisc').text(predio.penrodocumento);
                                                            $('#popup-nombreFisc').text(predio.penombres+" "+predio.peapellidopaterno+" "+predio.peapellidomaterno);
                                                            $('#popup-direccionFisc').text(predio.condomiciliofiscal);
                                                            if(predio.image == null){
                                                                $('#popup-imageFisc').hide();
                                                            }else{
                                                                $('#popup-imageFisc img').attr(src,"data:image/png;base64,"+predio.image);
                                                                $('#popup-imageFisc').show();
                                                            }
                                                        }
                                                })
                                                .catch(e => {
                                                    console.log("Error al intentar traer la información del lote: "+e)
                                                })
                                            }
                                        }
                                    };
                                    xhttp.open("GET", url, true);
                                    xhttp.send();

                                    
                                    var coordinate = evt.coordinate;
                                    popup.setPosition(coordinate);
                                }
                            })
                        } else if (element.tabla_capa == "sp_formularios") {
                            var source = new ol.source.ImageWMS({
                                url: geoserver+'/' + workSpace + '/wms',
                                params: { 'LAYERS': workSpace + ':' + element.tabla_capa, 'SRS': mSRS },
                                serverType: 'geoserver',
                                // Countries have transparency, so do not fade tiles:
                                transition: 0
                            });
                            
                            map.on('singleclick', function (evt) {
                                $('#popup, #popup-fiscalizacion, #popup-lote').hide();
                                var view = map.getView();
                                var viewResolution = view.getResolution();
                                var url = source.getGetFeatureInfoUrl(
                                    evt.coordinate, viewResolution, 'EPSG:900913',
                                    { 'INFO_FORMAT': 'application/vnd.ogc.gml', 'LAYERS': workSpace + ':sp_formularios', 'FEATURE_COUNT': 10 }
                                );
                                if (url) {
                                    var idexpediente = "";
                                    var xhttp = new XMLHttpRequest();
                                    xhttp.onreadystatechange = function () {
                                        if (this.readyState == 4 && this.status == 200) {
                                            var parser = new DOMParser();
                                            var xmlDoc = parser.parseFromString(this.response, "text/xml");
                                            if (xmlDoc.getElementsByTagName(workSpace + ":sp_formularios")[0] != undefined){
                                                var x = xmlDoc.getElementsByTagName(workSpace + ":sp_formularios")[0].getAttribute("fid");
                                                idexpediente = x.substr(15);
                                                $('#popup').show();
                                                $('#popup-idFisc')[0].setAttribute('data-foo', idexpediente);
                                                
                                                //axios.get(host+"/unidadcatlote?id="+id_lote) 03045032
                                                axios.get(host+"/expediente/map?id_expediente="+idexpediente)
                                                    .then(response => {
                                                        if(response.data.total == 1){
                                                            var expediente = response.data.data[0];
                                                            $('#popup-expediente').show();
                                                            $('#popup-nroExp').text(expediente.nro_expediente);
                                                            $('#popup-dniExp').text(expediente.nro_doc);
                                                            $('#popup-direccionExp').text(expediente.direccion);
                                                            $('#popup-nombreExp').text(expediente.administrado);
                                                        }
                                                })
                                                .catch(e => {
                                                    console.log("Error al intentar traer la información del lote: "+e)
                                                })
                                            }
                                        }
                                    };
                                    xhttp.open("GET", url, true);
                                    xhttp.send();

                                    
                                    var coordinate = evt.coordinate;
                                    popup.setPosition(coordinate);
                                }
                            })
                        } else if (element.nomb_capa == "sp_manzana") {
                            capaMapStore.data.mzna = la;
                        } else if (element.nomb_capa == "sp_sector") {
                            capaMapStore.data.sector = la;
                        }

                        controlCapaStore.data.capaMap.push({'capa':la, 'nombre':element.nomb_capa});

                    });

                    //capaMapStore.data.imp += capaMapStore.data.imp.substr(0, capaMapStore.data.imp.length-1);

                    setTimeout(function () {
                        $('.externalLayerLoad').hide();
                        map.updateSize();
                    }, 1000);
            
        },
        legendShow(nomb_capa, visible){
            for (let i = 0; i < controlCapaStore.data.capaVisible.length; i++) {
                if (controlCapaStore.data.capaVisible[i].nomb_capa == nomb_capa){
                    controlCapaStore.data.capaVisible[i].visible = visible;
                    //var capaVisible_new = controlCapaStore.data.capaVisible;
                    //controlCapaStore.data.capaVisible = capaVisible_new;
                }
            }
        }
    }
};
//http://localhost:8080/GlgisSMP/catastro/capasuser/cargar?id=gl_administrador