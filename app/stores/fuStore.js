const fuStore = {
    data: {
        expediente: {},
        errors: []
    },
    methods: {
    	loadExpediente(id_expediente) {
    		var expPromise = new Promise((resolve, reject) => {
	            axios.get(host+'/expediente/expediente?id_expediente='+id_expediente)
	            //http://localhost:8080/cugisMDT/expediente/expediente?id_expediente=100562889
	                .then(response => {
	                    // JSON responses are automatically parsed.
	                    if(response.data.data.length > 0){
	                    	fuStore.data.expediente = response.data.data[0];
	                    	resolve();
	                    }
	                    else{
	                    	reject("No hay expedientes");
	                    }
	                })
	                .catch(e => {
	                    fuStore.data.errors.push(e)
	                    reject(e);
	                })
	        });
	        return expPromise;
        }
    }
};