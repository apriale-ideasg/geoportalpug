//import axios from 'axios';

const cuMaestrosStore = {
	data: {
		departamentos: [],
		provincias: [],
		distritos: [],
		estado_civil: [],
		tipo_via: [],
		errors: []
	},
	methods: {
		getDepartamento(){
			var departamentoPromise = new Promise((resolve, reject) => {
				axios.get(host_test+'/departamento.json')
					.then(response => {
						cuMaestrosStore.data.departamentos = [];
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							cuMaestrosStore.data.departamentos.push(element);
						});
						resolve(); // fulfilled
					})
					.catch(e => {
						cuMaestrosStore.data.errors.push(e);
						reject();
					})
			})
			return departamentoPromise;
		},
		getProvincias(){
			var provinciaPromise = new Promise((resolve, reject) => {
				axios.get(host_test+'/provincia.json')
					.then(response => {
						// JSON responses are automatically parsed.
						cuMaestrosStore.data.provincias = [];
						response.data.data.forEach(element => {
							cuMaestrosStore.data.provincias.push(element);
						});
						resolve(); // fulfilled
					})
					.catch(e => {
						cuMaestrosStore.data.errors.push(e);
						reject();
					})
			})
			return provinciaPromise;
		},
		getDistritos(){
			var distritoPromise = new Promise((resolve, reject) => {
				axios.get(host_test+'/distrito.json')
					.then(response => {
						// JSON responses are automatically parsed.
						cuMaestrosStore.data.distritos = [];
						response.data.data.forEach(element => {
							cuMaestrosStore.data.distritos.push(element);
						});
						resolve(); // fulfilled
					})
					.catch(e => {
						cuMaestrosStore.data.errors.push(e);
						reject();
					})
			})
			return distritoPromise;
		},
		getEstadoCivil(){
			var estadoCivilPromise = new Promise((resolve, reject) => {
				axios.get(host_test+'/estado-civil.json')
					.then(response => {
						// JSON responses are automatically parsed.
						cuMaestrosStore.data.estado_civil = [];
						response.data.data.forEach(element => {
							cuMaestrosStore.data.estado_civil.push(element);
						});
						resolve(); // fulfilled
					})
					.catch(e => {
						cuMaestrosStore.data.errors.push(e);
						reject();
					})
			})
			return estadoCivilPromise;
		},
		getTipoVia(){
			cuMaestrosStore.data.tipo_via = [
	            "Anexo",
	            "Asentamiento Humano",
	            "Asociacion",
	            "Avenida",
	            "Calle",
	            "Circuito",
	            "Conj. Habit.",
	            "Cooperativa",
	            "Jirón",
	            "Malecón",
	            "Mercado",
	            "Paraje",
	            "Pasaje",
	            "Plaza",
	            "Prolongación",
	            "Vía"
	        ];
			// var tipoViaPromise = new Promise((resolve, reject) => {
			// 	axios.get(host_test+'/tipo-via.json')
			// 		.then(response => {
			// 			// JSON responses are automatically parsed.
			// 			cuMaestrosStore.data.tipo_via = [];
			// 			response.data.data.forEach(element => {
			// 				cuMaestrosStore.data.tipo_via.push(element);
			// 			});
			// 			resolve(); // fulfilled
			// 		})
			// 		.catch(e => {
			// 			cuMaestrosStore.data.errors.push(e);
			// 			reject();
			// 		})
			// })
			// return tipoViaPromise;
		}
	}
};


//export default rolStore;