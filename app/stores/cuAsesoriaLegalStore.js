const cuAsesoriaLegalStore = {
    data: {
        expedientes: [],
        select_expediente: [],
        errors: []
    },
    methods: {
    	loadExpedientes() {
            axios.get(host+'/formulario/asesoria')
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuAsesoriaLegalStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuAsesoriaLegalStore.data.expedientes.push(element);
                    });

                    $('.asesoriaLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        searchExpedientes(num_doc, nombap, n_exp) {
            axios.get(host+'/formulario/asesoria/b_expediente?num_doc='+num_doc+'&appnom='+nombap+'&nro_expediente='+n_exp)
            //axios.get(host_test+'/expedientes_s.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuAsesoriaLegalStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuAsesoriaLegalStore.data.expedientes.push(element);
                    });

                    $('.asesoriaLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        seleccionarExpediente(id_expediente) {
            axios.get(host+'/formulario/asesoria/expediente?id_expediente='+id_expediente)
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data.length > 0) {
                    	cuAsesoriaLegalStore.data.select_expediente = response.data.data[0];
                    }

                    $('.asesoriaLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        registerConformidad(expediente) {
            var registerConformidadPromise = new Promise((resolve, reject) => {
                axios.post(host+`/formulario/asesoria/update`, expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("Asesoria legal actualizada con exito");
                        $('.asesoriaLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuAsesoriaLegalStore.data.errors.push(e);
                    reject(e);
                    console.warn(e);
                })
            })
            return registerConformidadPromise;
        }
    }
};