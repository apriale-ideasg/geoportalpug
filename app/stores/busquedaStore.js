const busquedaStore = {
    data: {
        titular: [],
        sector: [],
        manzana: [],
        lote: [],
        errors: []
    },
    methods: {
        loadTitulares(nombre) {
            axios.get(host+`/catastro/consultar?appnombre=`+nombre)
                .then(response => {
                    // JSON responses are automatically parsed.
                    busquedaStore.data.titular = [];

                    response.data.data.forEach(element => {
                        busquedaStore.data.titular.push(element);
                    });

                    $('.searchTitularLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        loadSector() {
            axios.get(host+`/sector`)
                .then(response => {
                    // JSON responses are automatically parsed.
                    busquedaStore.data.sector = [];

                    response.data.data.forEach(element => {
                        busquedaStore.data.sector.push(element);
                    });

                    busquedaStore.data.manzana= [];
                    busquedaStore.data.lote= [];
                    $('.searchSpacialLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        loadManzana(id) {
            axios.get(host+`/manzana?id_sector=` + id)
                .then(response => {
                    // JSON responses are automatically parsed.
                    busquedaStore.data.manzana = [];

                    response.data.data.forEach(element => {
                        busquedaStore.data.manzana.push(element);
                    });
                    
                    busquedaStore.data.lote = [];
                    $('.searchSpacialLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        loadLote(id) {
            axios.get(host+`/lote?id_manzana=` + id)
                .then(response => {
                    // JSON responses are automatically parsed.
                    busquedaStore.data.lote = [];

                    response.data.data.forEach(element => {
                        busquedaStore.data.lote.push(element);
                    });

                    $('.searchSpacialLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        }
    }
};

//http://localhost:8080/GlgisSMP/catastro/listarsector
//http://localhost:8080/GlgisSMP/catastro/listarmanzana?id=46  id:46
//http://localhost:8080/GlgisSMP/catastro/listarlote?id=17423  id:17423