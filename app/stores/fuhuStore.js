//import axios from 'axios';

const fuhuStore = {
	data: {
		tipo_tramite: [],
		tipo_obra: [],
		modalidad_aprobacion: [],
		anexos: [],
		doc_adjuntos: [],
		edificacion: [],
		proyecto : {
            pisos : []
        },
		errors: [],
		show_id_expediente: '',
		expediente: {
			base : {},
			solicitud: {
				tipo: [],
				tipo_tramite: [],
				modalidad_aprobacion: [],
				anexos: []
			},
			propietario: {
				persona: {},
				domicilio: {},
				conyuge: {},
				propietario: {},
				empresa: {}
			},
			requisitos: [],
			terreno: {},
			proyecto: {
				proyectista: {},
				area: {}
			}
		}
	},
	methods: {
		getTipoTramite(){
			var tipoTramitePromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/tipo-tramite?id_tipo_formulario='+ '2')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fuhuStore.data.tipo_tramite.push(element);
						});
						$('.fuhuLoad').hide();
						resolve(); // fulfilled
						//rolStore.data.rols.push(response.data.data);
					})
					.catch(e => {
						fuhuStore.data.errors.push(e);
						reject();
					})
			})
			return tipoTramitePromise;
		},
		getTipoObra(){
			var tipoObraPromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/tipo?id_tipo_formulario='+ '2')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fuhuStore.data.tipo_obra.push(element);
						});
						$('.fuhuLoad').hide();
						resolve(); // fulfilled
						//rolStore.data.rols.push(response.data.data);
					})
					.catch(e => {
						fuhuStore.data.errors.push(e);
						reject();
					})
			})
			return tipoObraPromise;
		},
		getModalidadAprobacion(){
			var modalidadAprobPromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/modalidad-aprobacion')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fuhuStore.data.modalidad_aprobacion.push(element);
						});
						$('.fuhuLoad').hide();
						resolve(); // fulfilled
						//rolStore.data.rols.push(response.data.data);
					})
					.catch(e => {
						fuhuStore.data.errors.push(e);
						reject();
					})
			})
			return modalidadAprobPromise;
		},
		getAnexos(){
			var anexosPromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/anexos?id_tipo_formulario='+ '2')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fuhuStore.data.anexos.push(element);
						});
						$('.fuhuLoad').hide();
						resolve(); // fulfilled
						//rolStore.data.rols.push(response.data.data);
					})
					.catch(e => {
						fuhuStore.data.errors.push(e);
						reject();
					})
			})
			return anexosPromise;
		},
		getAdjuntos(){
			var adjuntosPromise = new Promise((resolve, reject) => {
				axios.get(host+'/formulario/documentos-adjuntos?id_tipo_formulario='+ '2')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fuhuStore.data.doc_adjuntos.push(element);
						});
						$('.fuhuLoad').hide();
						resolve(); // fulfilled
					})
					.catch(e => {
						fuhuStore.data.errors.push(e);
						reject();
					})
			})
			return adjuntosPromise;
		},
		getEdificacion(){
			var edificacionPromise = new Promise((resolve, reject) => {
				axios.get(host_test+'/fue-edificacion.json?id_tipo_formulario='+ '2')
					.then(response => {
						// JSON responses are automatically parsed.
						response.data.data.forEach(element => {
							fuhuStore.data.edificacion.push(element);
						});
						$('.fuhuLoad').hide();
						resolve(); // fulfilled
					})
					.catch(e => {
						fuhuStore.data.errors.push(e);
						reject();
					})
			})
			return edificacionPromise;
		},
		insertFuhu(expediente){
			var insertPromise = new Promise((resolve, reject) => {
				axios.post(host+`/formulario/insert-test`, expediente)
	            .then(response => {
	                // JSON responses are automatically parsed.
	                $('.fuhuLoad').hide();
	                resolve(response.data);
	                if (response.data.Result == "OK") {
	                    console.log("Form guardada con exito");
	                }
	                //$('.configLLoad').hide();
	            })
	            .catch(e => {
	                this.errors.push(e)
	                reject(e);
	            	console.log(this.errors);
	            })
            })
			return insertPromise;
		},
		buscarExpediente(){
			var insertPromise = new Promise((resolve, reject) => {
				var id_expediente = fuhuStore.data.show_id_expediente;

				axios.get(host+`/expediente/expediente?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.total > 0){
		                fuhuStore.data.expediente.base = response.data.data[0];
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

				axios.get(host+`/expediente/solicitud?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success){
		                fuhuStore.data.expediente.solicitud.tipo = response.data.tipo;
		            	fuhuStore.data.expediente.solicitud.anexos = response.data.anexos;
		            	fuhuStore.data.expediente.solicitud.tipo_tramite = response.data.tipo_tramite;
		            	fuhuStore.data.expediente.solicitud.modalidad_aprobacion = response.data.modalidadaprobacion;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/propietario?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success){
		                fuhuStore.data.expediente.propietario.persona = response.data.persona;
		            	fuhuStore.data.expediente.propietario.domicilio = response.data.domicilio;
		            	fuhuStore.data.expediente.propietario.conyuge = response.data.conyuge;
		            	fuhuStore.data.expediente.propietario.propietario = response.data.propietario[0];
		            	fuhuStore.data.expediente.propietario.empresa = response.data.empresa;
		            	fuhuStore.data.expediente.propietario.apoderado = response.data.apoderado;
		            	fuhuStore.data.expediente.propietario.apoderado_domicilio = response.data.apoderado_domicilio;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/docajuntos?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success){
		                fuhuStore.data.expediente.requisitos = response.data.data;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })
	            
	            axios.get(host+`/expediente/terreno?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.total > 0){
		                fuhuStore.data.expediente.terreno = response.data.data[0];
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/cuadroareas?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.total > 0){
		                fuhuStore.data.expediente.proyecto.area = response.data.data[0];
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })

	            axios.get(host+`/expediente/proyectista?id_expediente=`+id_expediente)
	            .then(response => {
		            if(response.data.success && response.data.total > 0){
		                fuhuStore.data.expediente.proyecto.proyectista = response.data.data;
		            }
	            })
	            .catch(e => {
	                this.errors.push(e)
	                //reject(e);
	            	console.log(this.errors);
	            })
            })
			return insertPromise;
		}
	}
};


//export default rolStore;