const cuAsignarTecnicoStore = {
    data: {
        expedientes: [],
        tecnicos: [],
        select_expediente: [],
        errors: []
    },
    methods: {
    	loadExpedientes() {
            axios.get(host+'/formulario/asignar')
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuAsignarTecnicoStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuAsignarTecnicoStore.data.expedientes.push(element);
                    });

                    $('.asignarTecnLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        loadTecnicos() {
            //axios.get(host+`/formulario/acotar)
            axios.get(host+'/formulario/tecnico')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuAsignarTecnicoStore.data.tecnicos = [];

                    response.data.data.forEach(element => {
                        cuAsignarTecnicoStore.data.tecnicos.push(element);
                    });

                    $('.asignarTecnLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        searchExpedientes(num_doc, nombap, n_exp) {
            axios.get(host+'/formulario/asignar/b_expediente?num_doc='+num_doc+'&appnom='+nombap+'&nro_expediente='+n_exp)
            //axios.get(host_test+'/expedientes_s.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuAsignarTecnicoStore.data.expedientes = [];

                    response.data.data.forEach(element => {
                        cuAsignarTecnicoStore.data.expedientes.push(element);
                    });

                    $('.asignarTecnLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        searchTecnicos(codigo, appnom) {
            axios.get(host+'/formulario/tecnico/b_tecnico?codigo='+codigo+'&appnom='+appnom)
            //axios.get(host_test+'/expedientes_s.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    cuAsignarTecnicoStore.data.tecnicos = [];

                    response.data.data.forEach(element => {
                        cuAsignarTecnicoStore.data.tecnicos.push(element);
                    });

                    $('.asignarTecnLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        seleccionarExpediente(id_expediente) {
            axios.get(host+'/formulario/asignar/expediente?id_expediente='+id_expediente)
            //axios.get(host_test+'/expedientes.json')
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.data.length > 0) {
                    	cuAsignarTecnicoStore.data.select_expediente = response.data.data[0];
                    }

                    $('.asignarTecnLoad').hide();
                })
                .catch(e => {
                    busquedaStore.data.errors.push(e)
                })
        },
        selectTecnicoSave(id_tecnico, id_expediente){
            var selectTecnicoPromise = new Promise((resolve, reject) => {
                let expediente = {
                    'id_tecnico' : id_tecnico,
                    'id_expediente' : id_expediente
                }
                axios.post(host+`/formulario/asignar/insert`, expediente)
                .then(response => {
                    // JSON responses are automatically parsed.
                    if (response.data.success == true) {
                        console.log("ASignación de tecnico actualizada con exito");
                        $('.asignarTecnLoad').hide();
                    }
                    resolve(response.data);
                })
                .catch(e => {
                    cuAsignarTecnicoStore.data.errors.push(e);
                    reject(e);
                    console.warn(e);
                })
            })
            return selectTecnicoPromise;
        },
        selectTecnicoEdit(id_tecnico, id_asignacion){
            var selectTecnicoPromise = new Promise((resolve, reject) => {
                resolve();
            })
            return selectTecnicoPromise;  
        }
    }
};