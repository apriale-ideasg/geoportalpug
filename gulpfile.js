var gulp = require('gulp');

var browserSync = require('browser-sync').create();

gulp.task('browserSync', function(){
	browserSync.init({
		server: {
			baseDir: 'app'
		},
	})
});

gulp.task('watch', ['browserSync'], function (){
  gulp.watch('app/*.html', browserSync.reload); 
  gulp.watch('app/js/**/*.js', browserSync.reload); 
  gulp.watch('app/**/*.js', browserSync.reload); 
  gulp.watch('app/**/*.css', browserSync.stream());
  //gulp.watch('app/css/*.css', browserSync.stream());
  // Other watchers
});